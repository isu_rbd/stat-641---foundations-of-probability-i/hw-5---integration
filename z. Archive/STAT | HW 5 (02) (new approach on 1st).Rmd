---
title: "STAT 641 - HW 5"
author: "Ricardo Batista"
date: "10/19/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Question 2.15

## (c)

&nbsp;&nbsp;&nbsp;&nbsp; First we show that $F(y) \geq x \iff F_1^{-1}(x) \leq y$.

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{F(y) \geq x \implies F_1^{-1}(x) \leq y}$

&nbsp;&nbsp;&nbsp;&nbsp; Since $F(y) \geq x$, then $y \in \{y \in \mathbb{R}: F(y) \geq x\}$. Note that $F_1^{-1}(x) = \inf \{y \in \mathbb{R} : F(y) \geq x\}$, therefore $F_1^{-1}(x) \leq y$.


&nbsp;&nbsp;&nbsp;&nbsp; $\underline{F(y) \geq x \impliedby F_1^{-1}(x) \leq y}$

&nbsp;&nbsp;&nbsp;&nbsp; Note that $y \geq F_1^{-1}(x) = \inf \{ y \in \mathbb{R}: F(y) \geq x\}$. Since cdfs are increasing functions, $y \geq F_1^{-1} \implies F(y) \geq F \left( F_1^{-1} (x)\right) = x$ since cdfs are right-continuous.  

### (i)

$$
\begin{aligned}
F_{Z_1}(z_1) &= P(Z_1 \leq z_1) = P\left( F_1^{-1} (X) \leq z_i\right)\\[10pt]
&= P\left( \inf \{ y : F(Y) \} \leq z_i\right)\\[10pt]
\end{aligned}
$$


### (ii)



# Question 2.20

&nbsp;&nbsp;&nbsp;&nbsp; Consider the sequence of nonnegative measurable functions $\{f_i\}_{i = 1}^{n}$ where

$$
f_i(x) = \sum_{j = 1}^{\infty} a_{ij} I_{[j, j + 1)}(x).
$$

then, by Corollary 2.3.5

$$
\sum_{i = 1}^\infty \int_{\mathbb{R}} f dm = \int_{\mathbb{R}} \sum_{i = 1}^\infty  f dm
$$

and

$$
\int_{\mathbb{R}} f dm = \sum_{j = 1}^{\infty} a_{ij} m([j, j + 1]) = \sum_{j = 1}^{\infty} a_{ij}.
$$

For the RHS we have

$$
\sum_{i = 1}^{\infty} f_i (x)
= \sum_{j = 1}^{\infty} \left[ \left(\sum_{i = 1}^\infty a_{ij}\right) I_{[j, j + 1)}(x)\right].
$$

Then, by Corollary 2.3.5 once more,

$$
\begin{aligned}
\int_{\mathbb{R}} \sum_{i = 1}^{\infty} f dm 
&= \int_{\mathbb{R}} \sum_{j = 1}^{\infty} \left[ \left(\sum_{i = 1}^\infty a_{ij}\right) I_{[j, j + 1)}(x)\right] dm\\[10pt]
&= \sum_{j = 1}^{\infty} \int_{\mathbb{R}} \left[ \left(\sum_{i = 1}^\infty a_{ij}\right) I_{[j, j + 1)}(x)\right] dm\\[10pt]
&= \sum_{j = 1}^{\infty} \left[ \sum_{i = 1}^{\infty} a_{ij}\right].
\end{aligned}
$$

Thus, we have shown that 

$$
\sum_{i = 1}^{\infty} \left[ \sum_{j = 1}^{\infty} a_{ij}\right] = \sum_{j = 1}^{\infty} \left[ \sum_{i = 1}^{\infty} a_{ij}\right].
$$

# Question 2.25

&nbsp;&nbsp;&nbsp;&nbsp; Let $P = \{x_0, ..., x_n\}$ be a finite partition of $[0,1]$, i.e., $0 = x_0 < ... < x_n = 1$, and recall that 

$$
\begin{aligned}
L(f, P) &= \sum_{i = 1}^{n - 1} \inf\{I_{\mathbb{Q}_1} (x) : x \in [x_{i}, x_{i + 1}]\} \cdot(x_{i + 1} - x_{i})\\[10pt]
U(f, P) &= \sum_{i = 1}^{n - 1} \sup\{I_{\mathbb{Q}_1} (x) : x \in [x_{i}, x_{i + 1}]\} \cdot(x_{i + 1} - x_{i}).
\end{aligned}
$$

Note note that since $\mathbb{Q}$ is dense in $\mathbb{R}$, there exists $r \in \mathbb{Q}$ such that $x_i < r_i < x_{i + 1}$. Therefore,

$$
\begin{aligned}
U(f, P) &= \sum_{i = 1}^{n - 1} I_{\mathbb{Q}_1} (r_i) \cdot(x_{i + 1} - x_{i})\\[10pt]
 &= \sum_{i = 1}^{n - 1} 1 \cdot(x_{i + 1} - x_{i})\\[10pt]
&= (x_1 - x_0) + (x_2 - x_1) + \cdots + (x_{n - 1} - x_{n - 2}) + (x_n - x_{n - 1})\\[10pt]
& = x_n - x_0 = 1 - 0 = 1.
\end{aligned}
$$

Similarly, for any $x_i, x_{i + 1}$ we have that

$$
a_i = x_i + \frac{x_{i + 1} - x_i}{\sqrt{2}} \in \mathbb{R} \setminus \mathbb{Q}
$$

and note $x_i < a_i < x_$. Therefore,

$$
\begin{aligned}
L(f, P) &= \sum_{i = 1}^{n - 1} I_{\mathbb{Q}_1} (a_i) \cdot(x_{i + 1} - x_{i})\\[10pt]
 &= \sum_{i = 1}^{n - 1} 0 \cdot(x_{i + 1} - x_{i}) = 0.
\end{aligned}
$$

Suppose that $Q = \{y_0, ..., y_k\}$ is another partition such that $P \subset Q$, then

$$
U(f, P) = U(f, Q) = 1 > 0 = L(f, Q) = L(f, P).
$$

Therefore, letting $\mathcal{P}$ denote the collection of all finite partitions on $[0, 1]$, 

$$
\bar{\int} f = \inf_{P \in \mathcal{P}} U(f, P)  = 1 \neq 0 = \sup_{P \in \mathcal{P}} L(f, P) =\int_{\bar{}} f
$$

so $f$ is not Riemann integrable, i.e., the Riemann integral for $f$ does not exist. 

&nbsp;&nbsp;&nbsp;&nbsp; Now consider the Lebesgue integral of $f$ w.r.t. the Lebesgue measure $\mu$. Note that $f(x) = 1$ when $x \in \mathbb{Q}$, but $\mu (\{x\}) = F(x+) - F(x-) = 0$ (where $F(x) = x$ by definition of Lebesgue measure), so

$$
\int f d\mu = 0.
$$


# Additional Problems | A


## (i)

&nbsp;&nbsp;&nbsp;&nbsp; Fix $n$. Let 

$$
\begin{aligned}
A_i &:= \left[\frac{i - 1}{2^n}, \frac{i}{2^n}\right) && \text{for } i = 1, ..., n2^n\\[10pt]
A_{n2^n + 1} &:= n.
\end{aligned}
$$

Note that $\{A_i\}_{i = 1}^{n2^n + 1} \subset \mathcal{B}(\mathbb{R})$ and, since $f$ is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable, $f^{-1}(A_i) \in \mathcal{B}(\mathbb{R})$.

&nbsp;&nbsp;&nbsp;&nbsp; Now consider $B \in \mathcal{B}(\mathbb{R})$ such that it contains some

$$
\frac{i - 1}{2^n} \in \left\{\frac{i - 1}{2^n}\right\}_{i = 1}^{n2^n + 1}
$$ 

Let

$$
I = \left\{ i : \frac{i - 1}{2^n} \in B \right\},
$$

then

$$
\begin{aligned}
f_{n}^{-1}(B) &= \left\{x \in \Omega: f_n(x) \in B\right\}\\[10pt]
&= \bigcup_{i \in I} \left\{x \in \Omega: f_n(x) = \frac{i - 1}{2^n} \right\}\\[10pt]
&= \bigcup_{i \in I} \left\{x \in \Omega: f(x) \in A_i \right\}\\[10pt]
&= \bigcup_{i \in I} f^{-1}(A_i)\\[10pt]
\end{aligned}
$$

and $\bigcup_{i \in I} f^{-1}(A_i) \in \mathcal{F}$ since it is a countable union of sets and $\mathcal{F}$ is a $\sigma$-algebra.  

## (ii)

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\text{Case } 1}: f(x) \geq n + 1$

$$
f_{n + 1}(x) - f_{n}(x) = (n + 1) - n = 1 > 0
$$

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\text{Case } 2}: f(x) < n$

&nbsp;&nbsp;&nbsp;&nbsp; The inequalities 

$$
\begin{aligned}
\frac{i - 1}{2^n} \leq f(x) < \frac{i}{2^n}\\[10pt]
i - 1 \leq 2^nf(x) < i
\end{aligned}
$$

reveal that

$$
f_n(x) = \frac{\lfloor 2^n f(x) \rfloor}{2^n}.
$$

Then,

$$
\begin{aligned}
f_{n + 1} (x) 
= \frac{\lfloor 2^{n + 1} f(x) \rfloor}{2^{n + 1}} 
= \frac{\lfloor 2 \cdot 2^{n} f(x) \rfloor}{2^{n + 1}}
\geq \frac{2 \lfloor 2^{n} f(x) \rfloor}{2^{n + 1}} 
= \frac{\lfloor 2^{n} f(x) \rfloor}{2^{n}} = f_n(x)
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\text{Case } 3}: n \leq f(x) < n + 1$

&nbsp;&nbsp;&nbsp;&nbsp; Note that $f_n(x) = n$. Additionally, since $n \leq f(x)$, $n \leq \lfloor f(x) \rfloor$. Then, from above

$$
f_{n + 1}(x)
\geq \frac{\lfloor 2^{n} f(x) \rfloor}{2^{n}} 
\geq \frac{2^{n}\lfloor f(x) \rfloor}{2^{n}}
= \lfloor f(x) \rfloor
\geq n = f_n(x).
$$

## (iii)

&nbsp;&nbsp;&nbsp;&nbsp; Fix $x$ and let $\epsilon > 0$ be given. Then, select $N\varepsilon$ such that $N\varepsilon > f(x)$ and

$$
\frac{1}{2^{N\varepsilon}} < \epsilon.
$$

then

$$
\frac{1}{2^{N\varepsilon}} 
\geq \frac{\vert 2^nf(x) - \lfloor 2^n f(x) \rfloor\vert}{2^n}
= \left \vert \frac{2^nf(x) - \lfloor \min(2^n f(x), 2^nn) \rfloor}{2^n} \right \vert
= \vert f(x) - f_n (x) \vert.
$$

# Additional Problems | B

&nbsp;&nbsp;&nbsp;&nbsp; First not that 

$$
\min(f, g) \leq \vert f \vert + \vert g \vert \implies \min(f, g) \in L_1(\Omega, \mathcal{F}, \mu).
$$


&nbsp;&nbsp;&nbsp;&nbsp; Now note that 

$$
\min(c, d) = \frac{c + d - \vert c - d \vert}{2}
$$

so we have

$$
\begin{aligned}
2 \left\{ \min \left( \int f d\mu, \int g d\mu \right) - \int \min(f,g) \right\}
&= \int (f + g) d\mu - \left\vert \int (f - g) d\mu \right\vert - \int (f + g) d\mu + \int \vert f - g \vert d\mu \\[10pt]
&= \int \vert f - g \vert d\mu - \left\vert \int (f - g) d\mu \right\vert \geq 0.
\end{aligned}
$$


